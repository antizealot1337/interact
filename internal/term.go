// +build !linux, !bsd, !osx

package internal

import (
	"os"
	"syscall"
)

var (
	sttyAttr syscall.ProcAttr
)

func init() {
	sttyAttr.Files = []uintptr{os.Stdin.Fd(), os.Stdout.Fd(), os.Stderr.Fd()}
} //func

// TermEcho turns echoing on/off.
func TermEcho(on bool) error {
	// The argument for stty
	var arg string

	// Determine the argument
	if on {
		arg = "echo"
	} else {
		arg = "-echo"
	} //if

	// Execute the echoing command
	pid, err := syscall.ForkExec("/bin/stty", []string{"stty", arg}, &sttyAttr)

	// Check for an error
	if err != nil {
		return err
	} //if

	// The wait status for the process
	var wstatus syscall.WaitStatus

	// Wait for the process to finish
	_, err = syscall.Wait4(pid, &wstatus, 0, nil)

	return err
} //func
