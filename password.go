package interact

import "bitbucket.org/antizealot1337/interact/internal"

// Password retrieves a password from stdin. It will disable echoing what the
// user types in. On unsupported platforms it just calls Prompt.
func Password(prompt string) (string, error) {
	// Disable terminal echoing
	if err := internal.TermEcho(false); err != nil {
		return "", err
	} //if

	// Make sure terminal echoing is truned back on
	defer func() {
		internal.TermEcho(true)
	}()

	// Get the input
	return Prompt(prompt)
} //func
