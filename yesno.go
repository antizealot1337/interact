package interact

import "fmt"

const (
	defaultYesString = "(Y/n)"
	defaultNoString  = "(y/N)"
)

// YesNo displays the message and then prompts the use for a yes or no response.
// An affirmative response can be any of "Yes", "yes", "Y", or "y" while a
// negative response can be any of "No", "no", "N", "n". The returned boolean
// will be either true for yes or false for no. Any read errors are returned.
func YesNo(message string, defaultYes bool) (bool, error) {
	// The string for the defaults
	var ynStr string

	// Check if the default is yes or no
	if defaultYes {
		ynStr = defaultYesString
	} else {
		ynStr = defaultNoString
	} //if

	// Prompt the user for input
	choice, err := Prompt(fmt.Sprintf("%s %s: ", message, ynStr))

	// Check for an error
	if err != nil {
		return defaultYes, err
	} //if

	// Determine what to return
	switch choice {
	case "YES", "Yes", "yes", "Y", "y":
		return true, nil
	case "NO", "No", "no", "N", "n":
		return false, nil
	default:
		return defaultYes, nil
	} //switch
} //YesNo
