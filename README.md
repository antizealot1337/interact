# Interact [![GoDoc](https://godoc.org/bitbucket.org/antizealot1337/interact?status.svg)](https://godoc.org/bitbucket.org/antizealot1337/interact)

Interact is a library intended to make interactive command line applications
simpler to develop. Functionality is added on an as needed basis so it is not
"fully featured". If functionality is needed don't hesitate to ask or fork this
repository. Pull requests are always appreciated

## Example
```
```

## License
Licensed under the terms of the MIT license. Refer to the LICENSE file for more
details.
