package interact

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// Prompt shows the message and then waits for input. If an error is encountered
// it wil be returned.
func Prompt(message string) (string, error) {
	// Write the message
	fmt.Print(message)

	// Create a Reader
	reader := bufio.NewReader(os.Stdin)

	// Wait for the user to respond
	input, err := reader.ReadString('\n')

	// Check for an error
	if err != nil {
		return input, err
	} //if

	// Remove white space from the input
	return strings.TrimSpace(input), nil
} //Prompt

// PromptInt64 shows the message to the user and attempts to read an int64 from
// input.
func PromptInt64(message string) (int64, error) {
	// Prompt the user
	sval, err := Prompt(message)

	// Check for an error
	if err != nil {
		return 0, err
	} //if

	// Attempt to parse the int
	return strconv.ParseInt(sval, 10, 64)
} //PromptInt64

// PromptFloat64 shows the message to the user and attempts to read an float64
// from input.
func PromptFloat64(message string) (float64, error) {
	// Prompt the user
	sval, err := Prompt(message)

	// Check for an error
	if err != nil {
		return 0, err
	} //if

	// Attempt to parse the float
	return strconv.ParseFloat(sval, 64)
} //PromptFloat64
