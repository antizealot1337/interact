package interact

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
)

type stringer interface {
	String() string
}

// Menu will display the message followed by the options appearing on seperate
// lines with a number the user will enter to choose that option. If canQuit is
// true the a "q" will quit the Menu returning nil instead of one of the
// options. The position and choice will be returned and any errors that occur.
// Options is an interface but should be a slice of values.
func Menu(message string, options interface{}, canQuit bool) (int, interface{}, error) {
	// Get the options type
	otype := reflect.TypeOf(options)

	// Make it is a slice
	if otype.Kind() != reflect.Slice {
		return -1, nil, errors.New("The options should be a slice")
	} //if

	// The value of the options
	oval := reflect.ValueOf(options)

	for {
		fmt.Println(message)

		// Check if the user can quit
		if canQuit {
			fmt.Println("q: Quit")
		} //canQuit

		// Loop through the options
		for i := 0; i < oval.Len(); i++ {
			// Get the item
			item := oval.Index(i)

			if str, ok := item.Interface().(stringer); ok {
				fmt.Printf("%d: %s\n", i+1, str.String())
			} else {
				// Print the default output of the value
				fmt.Printf("%d: %v\n", i+1, item)
			} //if
		} //for

		// Get the user's choice
		input, err := Prompt("Enter choice: ")

		// Check for an error
		if err != nil {
			return -1, nil, err
		} //if

		// Check if we can quit
		if canQuit {
			// Check if the input is either "q" or "quit"
			if input == "q" || input == "quit" {
				return -1, nil, nil
			} //if
		} //if

		// Parse the value
		val, err := strconv.Atoi(input)

		// Check for an error
		if err != nil {
			return -1, nil, err
		} //if

		// Reduce the value
		val--

		// Make sure it is within the range of the options
		if val < 0 || val >= oval.Len() {
			fmt.Printf("Invalid index %d\n", val)
			continue
		} //if

		return val, oval.Index(val).Interface(), nil
	} //for
} //Menu
